const button = document.querySelector("button");
button.addEventListener("click",(event)=>{
    event.preventDefault();
    createCharacter();
})

const createCharacter = async () => {
    const name = document.querySelector("input[placeholder='Name']").value;
    const location = document.querySelector("input[placeholder='Location']").value;
    const quote = document.querySelector("input[placeholder='Quote']").value;

    console.log(name, location, quote);

    const data = {
        name: name,
        location: location,
        quote: quote
    }

    const result = await fetch("https://safe-gorge-61722.herokuapp.com/characters",{
        method: "POST",
        headers:{
            "Accept":"application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    })

    console.log(result);
}


/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass	http://dontpad.com/ajaxClass
/////////////////////////////////////////////////////////////////////////////////////////////////////////*/