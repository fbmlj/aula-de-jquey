const createTable = (characters) => {
	const tbody=document.querySelector('tbody');
	for (let i = 0;i < characters.length; i++){
		let character=characters[i]
		tbody.innerHTML += `<tr><td>${character.name}</td><td>${character.location}</td><td>${character.quote}</td></tr>`


	}
}



const getCharacters = async () => {
	const result = await fetch("https://safe-gorge-61722.herokuapp.com/characters")
	
	const json = await result.json();
	console.log(json);
	createTable(json);
}

// $.post('https://safe-gorge-61722.herokuapp.com/characters',{name: 'oi'}).fail(alert('erro'))
// getCharacters();