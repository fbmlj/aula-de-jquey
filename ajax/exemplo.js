const getTicket = new Promise((resolve, reject) => {
    setTimeout(()=>{
        resolve(`ticket`);
    }, 1000)
});

const getPopcorn = (result) => {
    return new Promise((resolve, reject)=>{
        resolve(`pipoca`)
    })
}

const addButter= (result) => {
    return new Promise((resolve, reject)=>{
        resolve(`manteiga`)
    })
}

const preMovie = async () => {
	const ticket = await getTicket;
	const popcorn= await getPopcorn();
	const butter = await addButter();
	console.log(`pessoa 4: Está com ${ticket}, ${popcorn} e ${butter}`)
}

console.log("pessoa 1: entrega o ingresso");
console.log("pessoa 2: entrega o ingresso");
console.log("pessoa 3: entrega o ingresso");
console.log("pessoa 4: ESQUECEU O INGRESSO!");
getTicket
.then(
    preMovie
    )

console.log("pessoa 5: entrega o ingresso");
console.log("pessoa 6: entrega o ingresso");
console.log("pessoa 7: entrega o ingresso");